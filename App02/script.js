function changecolor(){
    var hex_num = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"];

    var hexcode = "";

    for(var i=0; i<6; i++){
        var rand_index = Math.floor(Math.random()*hex_num.length);
        hexcode += hex_num[rand_index];
    }
    document.getElementById("hexcode").innerHTML = hexcode;
    document.getElementsByTagName("body")[0].style.background = "#"+hexcode;

}