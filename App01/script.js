var index = 0;

function changeColor(){
    // create-colors-in-a-array
    var colors = ["red", "orange", "yellow", "green", "blue", "indigo", "purple"];

    // access-body-tag
    document.getElementsByTagName("body")[0].style.background = colors[index++];

    if(index > colors.length-1)
        index = 0;
}